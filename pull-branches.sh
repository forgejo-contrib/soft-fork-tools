#!/usr/bin/env bash

source $(dirname $0)/common.sh

function pull_branch() {
    local branch=$1
	$DRY_RUN git update-ref -m 'Updating branch after rebase onto upstream' --create-reflog refs/heads/$branch refs/remotes/$REMOTE/$branch
}

function pull_branches() {
    local work=${1:-.}
    (
        cd $work

        if test -n "$(git status --porcelain)" ; then
            echo "You have uncommitted or unstaged changes. Please stash or commit before continuing."
            exit 1
        fi

        git fetch $REMOTE
        local current=$(git branch --show-current)

        for branch in $SOFTWARE $FEATURES ; do
            pull_branch $branch

            if test $branch == $current ; then
                $DRY_RUN git reset --hard HEAD
            fi
        done
    )
}
