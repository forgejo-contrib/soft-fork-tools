In support of [the documented Forgejo workflow](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/CONTRIBUTING/WORKFLOW.md).

# Dry run

`DRY_RUN=dry_run REMOTE=forgejo bash ~/software/forgejo/soft-fork-tools/main.sh archive_branches` and copy paste the output in a shell

# Archive branches

`REMOTE=forgejo bash ~/software/forgejo/soft-fork-tools/main.sh archive_branches`

# Rebase branches

`OWNER=forgejo NAME=forgejo FORGEJO_INSTANCE=codeberg.org FORGEJO_SCHEME=https FORGEJO_TOKEN=$rebasetoken REMOTE=forgejo bash ~/software/forgejo/soft-fork-tools/main.sh rebase_feature_branch v1.18/forgejo-development`
