#!/usr/bin/env bash

set -e

source $(dirname $0)/archive-branches-test.sh
source $(dirname $0)/api-test.sh
source $(dirname $0)/rebase-test.sh

trap "test_deactivate_trace ; teardown" EXIT

teardown
setup

function run() {
    for t in "$@"; do
	echo "===================== TEST $t BEGINS ====================="
	$t
	echo "===================== TEST $t ENDS ====================="
    done
}
    
run "$@"
