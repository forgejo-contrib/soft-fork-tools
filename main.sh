#!/usr/bin/env bash

set -e

source $(dirname $0)/rebase.sh
source $(dirname $0)/archive-branches.sh
source $(dirname $0)/pull-branches.sh

function main() {
    echo help
}

${@:-main}
