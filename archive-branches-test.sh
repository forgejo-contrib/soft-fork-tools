#!/usr/bin/env bash

source $(dirname $0)/common-test.sh
source $(dirname $0)/archive-branches.sh

function archive_branches_test_show_branch() {
    local archive=$1
    local branch=$2
    local archived_branch=$archive/$branch

    echo $archived_branch
}

function archive_branches_tests() {
    test_repository_create work

    (
	local work=$TMP_DIR/work
	local expected_visible=$TMP_DIR/expected_visible
	local expected_hidden=$TMP_DIR/expected_hidden
	cd $work

	test -z "$(git_visible_soft_fork)"
	test -z "$(git_hidden_soft_fork)"

	JANUARY="2022-01-20"
	TODAY=$JANUARY
	archive_action_on_branches archive_branches_test_show_branch | sort > $expected_visible$JANUARY

	for i in 1 2 ; do # idempotent
	    echo "===================== idempotent january pass $i ================="
	    archive_branches $work

	    git_visible_soft_fork | sort > $actual_visible$JANUARY
	    diff -u $actual_visible$JANUARY $expected_visible$JANUARY
	    test -z "$(git_hidden_soft_fork)"
	done

	FEBRUARY="2022-02-12"
	TODAY=$FEBRUARY
	archive_action_on_branches archive_branches_test_show_branch | sort > $expected_visible$FEBRUARY
	cat $expected_visible$JANUARY | sort > $expected_hidden$FEBRUARY

	for i in 1 2 ; do # idempotent
	    echo "===================== idempotent february pass $i ================="
	    archive_branches $work

	    git_visible_soft_fork | sort > $actual_visible$FEBRUARY
	    diff -u $actual_visible$FEBRUARY $expected_visible$FEBRUARY

	    git_hidden_soft_fork | sort > $actual_hidden$FEBRUARY
	    diff -u $actual_hidden$FEBRUARY $expected_HIDDEN$FEBRUARY
	done

	MARCH="2022-03-04"
	TODAY=$MARCH
	archive_action_on_branches archive_branches_test_show_branch | sort > $expected_visible$MARCH
	cat $expected_visible$JANUARY $expected_visible$FEBRUARY | sort > $expected_hidden$MARCH

	for i in 1 2 ; do # idempotent
	    echo "===================== idempotent march pass $i ================="
	    archive_branches $work

	    git_visible_soft_fork | sort > $actual_visible$MARCH
	    diff -u $actual_visible$MARCH $expected_visible$MARCH

	    git_hidden_soft_fork | sort > $actual_hidden$MARCH
	    diff -u $actual_hidden$MARCH $expected_hidden$MARCH
	done
    )
}
